/////////////////////////////////////////////////////////////////////////////////////////////
//
// allume-request-gitlab
//
//    Request module for fetching releases from GitLab.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
//
// Designed to accept the following requests:
//    git+https://gitlab.com/create-conform/allume-request-gitlab.git#semver:^1.0
//    https://gitlab.com/create-conform/allume-request-gitlab.git#semver:^1.0
//    https://gitlab.com/create-conform/allume-request-gitlabt#semver:^1.0
//    https://gitlab.com/create-conform/allume-request-gitlab
//
///////////////////////////////////////////////////////////////////////////////////////////// 
//
// Privates
//
///////////////////////////////////////////////////////////////////////////////////////////// 
var version =  require("version");
var string =   require("string-validation");
var config =   require("config");
var ioURI =    require("io-uri");
var ioAccess = require("io-access");

var REQUEST_PROC_NAME =                  "gitlab";
var HOST_GITLAB =                        "gitlab.com";
var URI_PATH_GITLABAPI_TAGS_TEMPLATE =   "%2F$NAME/repository/tags";
var URI_PATH_GITLABAPI_BRANCH_TEMPLATE = "%2F$NAME/repository/archive?sha=";
var PATH_CACHE =                         "allume-request-gitlab/cache/";
var EXT_PKX =                            "pkx";
var HTTP_HEADERS =                       { "user-agent" : "allume" };

///////////////////////////////////////////////////////////////////////////////////////////// 
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////////////////// 
function request() {
    if (selector.uri.authority.host != HOST_GITLAB) {
        return;
    }

    
    return new Promise(function (resolve, reject) {
        // remove git+ from protocol
        //TODO

        // strip first character
        selector.uri.path = selector.uri.path.substr(1);

        // strip slash at tail
        selector.uri.path = selector.uri.path.lastIndexOf("/") == selector.uri.path.length - 1? selector.uri.path.substr(0, selector.uri.path.length - 2) : selector.uri.path;

        // remove .git in repo name
        selector.uri.path = selector.uri.path.lastIndexOf(".git") == selector.uri.path.length - 4? selector.uri.path.substr(0, selector.uri.path.length - 5) : selector.uri.path;

        // replace path with gitlab api path
        selector.uri.path = "/api/v4/projects/" + selector.uri.path.replace(/\//g, "%2F");

        // get options
        var options = {
            "enableCache" : true
        };

        // prepare repository and user name
        var repoName =     selector.uri.path.substr(selector.uri.path.lastIndexOf("/", selector.uri.path.length - 2)).split("%2F");
        var userName =     repoName[0].substr(1);
            repoName =     repoName[1];
        var archiveURL;
        var semverFilter = "";
        var uri;

        // change repository name in api url
        selector.uri.path = "/api/v4/projects/" + userName + (URI_PATH_GITLABAPI_TAGS_TEMPLATE).replace(/\$NAME/g, repoName);
    
        // get the semver filter from the uri
        if (selector.uri.fragment) {
            if (selector.uri.fragment.substr(0,7) != "semver:") {
                throw "Unsupported URI fragment '" + selector.uri.fragment + "'.";
            }

            semverFilter = selector.uri.fragment.substr(7);
        }

        var archiveURL;
        var gitLabTag;
        var cacheURI;
        var cacheStream;
        var cacheVolume;
        var repoStream;
        var uriTags = selector.uri;
            uriTags.open()
                .then(readGitLabReleaseTagsStream)
                .then(processGitLabReleaseTags)
                .catch(findReleaseInLocalCache)
                .then(findReleaseInLocalCache)
                .catch(resolveURI);

        function readGitLabReleaseTagsStream(stream) {
            stream.headers = headers;
            return stream.readAsJSON();
        }
        function processGitLabReleaseTags(tags) {
            return new Promise(function(resolve, reject) {
                if (!tags || tags.length == 0) {
                    throw new Error("Package '" + selector.package + "' does not have any tags in the GitLab repository.");
                }
                var versions = [];
                var count = 0;
                for (var r in tags) {
                    var tagName = tags[r].name;
                    if (tagName.substr(0,1) == "v") {
                        tagName = tagName.substr(1);
                    }
    
                    //TODO
                    // CHECK SEMANTIC VERSION -> VALID
    
                    versions[tagName] = tags[r];
                    count++;
                }
                if (count == 0) {
                    throw new Error("Package '" + selector.package + "' does not contain one or more valid tags in the GitLab repository.");
                }
    
                var tag = version.find(versions, "", selector.upgradable || version.UPGRADABLE_NONE);

                resolve(tag);
            });
        }
        function findReleaseInLocalCache(tag) {
            return new Promise(function(resolve, reject) {
                if (tag instanceof Error) {
                    //console.error(release);
                    tag = null;
                }
    
                // variable will contain error message when download of tarball url fails.
                var tagErr;
    
                if (tag) {
                    gitLabTag = tag;
                    archiveURL = selector.parseURI("https://" + HOST_GITLAB + "/api/v4/projects/" + userName + "%2F" + repoName + "/repository/archive?sha=" + tag.name).toString();
                }
    
                if (options.enableCache) {
                    return config.getVolume()
                        .then(getCachedFilesFromLocalConfigVolume)
                        .catch(processCachedFilesResults)
                        .then(processCachedFilesResults)
                        .catch(resolveURI)
                        .then(resolveURI);
                }
                else {
                    return resolveURI(archiveURL);
                }
            });
        }
        function getCachedFilesFromLocalConfigVolume(volume) {
            cacheVolume = volume;
            return cacheVolume.query(PATH_CACHE + selector.repository.namespace + (selector.repository.namespace != ""? "/" : ""));
        }
        function processCachedFilesResults(uriList) {
            return new Promise(function(resolve, reject) {
                if (uriList && uriList.code == "ENOENT") {
                    uriList = [];
                }
                else if (uriList && uriList.code) {
                    throw new Error("Cache disk error.", uriList);
                }

                var cache = {};
                for (var u in uriList) {
                    if (uriList[u].path.lastIndexOf("/") != uriList[u].path.length - 1) {
                        var file = uriList[u].path.substr(uriList[u].path.lastIndexOf("/") + 1);
                        cache[file.substr(0,file.length - 4)] = uriList[u];
                    }
                }

                // get highest version from cache
                var highestCache = version.find(cache, semverFilter);

                if (!archiveURL) {
                    // resolve highest cache version
                    return resolve(highestCache);
                }
                else {
                    var id = repoName + "." + (gitLabTag.name.indexOf("v") == 0? gitLabTag.name.substr(1) : gitLabTag.name);
                    var found;
                    for (var u in cache) {
                        if (u == id) {
                            found = u;
                            break;
                        }
                    }
                    if (found) {
                        // release version from GitLab is present in cache
                        return resolve(cache[found]);
                    }
                    else {
                        // download new uri and save to cache
                        return ioURI.open(archiveURL)
                            .then(openCacheStreamForWriting)
                            .then(writeToCacheStream)
                            .catch(closeCacheStream)
                            .then(closeCacheStream)
                            .catch(closeRepositoryStream)
                            .then(closeRepositoryStream)
                            .catch(resolveCacheURIOrGitLabURI)
                            .then(resolveCacheURIOrGitLabURI)
                            .catch(resolveURI);
                    }
                }
            });
        }
        function openCacheStreamForWriting(repoStream) {
            cacheURI = cacheVolume.getURI(PATH_CACHE + id + "." + EXT_PKX);
            return cacheURI.open(ioAccess.OVERWRITE, true);
        }
        function writeToCacheStream(cacheStream) {
            repoStream.headers = HTTP_HEADERS;
            return repoStream.copyTo(cacheStream);
        }
        function closeRepositoryStream(e) {
            if (e instanceof Error) {
                tagErr = e;
            }
            return repoStream.close();
        }
        function closeCacheStream(e) {
            return cacheStream.close(tagErr);
        }
        function resolveCacheURIOrGitLabURI() {
            if (tagErr) {
                // an error occurred while downloading the tarball (could be CORS), fallback to highest cached version.
                resolveURI(highestCache);
                return;
            }
            resolveURI(cacheURI);
        }
        function resolveURI(uri) {
            return new Promise(function(resolve, reject) {
                if (uri && uri.name) {
                    reject(new Error("An error occured while trying to fetch '" + selector.package + "' from the GitLab repository."));
                    return;
                }
                else if (!uri && !tagErr) {
                    reject(new Error("Couldn't find any suitable tag for package '" + selector.package + "' in the GitLab repository."));
                    return;
                }
                else if (!uri && tagErr) {
                    if (!tag) {
                        reject(new Error("Downloading of package '" + selector.package + "' from GitLab failed. If you are running this in a browser, CORS might be the problem."));
                    }
                    return;
                }
                try {
                    selector.uri = uri;
                    resolve({
                        "strip"   : 1,
                        "headers" : headers
                    });
                }
                catch (e) {
                    reject(e);
                }
            });
        }
    });
}

///////////////////////////////////////////////////////////////////////////////////////////// 
//
// Register Allume CLI Options
//
///////////////////////////////////////////////////////////////////////////////////////////// 
//TODO

///////////////////////////////////////////////////////////////////////////////////////////// 
//
// Register Request Processor
//
///////////////////////////////////////////////////////////////////////////////////////////// 
define.Loader.waitFor("pkx", function(loader) {
    loader.addRequestProcessor(REQUEST_PROC_NAME, request);
});

///////////////////////////////////////////////////////////////////////////////////////////// 
module.exports = request;